
def calculations(x):
    return x * x

def test_answer():
    assert calculations(2) == 4
    assert calculations(3) == 9
